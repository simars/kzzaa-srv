'use strict';
const _ = require('lodash');
const env = require('./environment');

module.exports = _.merge({
    server: {
        host: '0.0.0.0',
        port: 8080,
        cors: false,
        clustered: 'redis',
        link: process.env.LINK // public host-name
    },
    redis: {
        host: 'redis',
        port: 6379
    },
    mongodb: {
        url: 'mongodb://mongo:27017/k',
        options: {
            useMongoClient: true
        },
        username: 'admin',
        password: 'password'
    },
    staticContentDir : process.env.STATIC_CONTENT_DIR
}, env);
