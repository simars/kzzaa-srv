'use strict';

const mapping = {
  prod: 'prod',
  production: 'prod',
  dev: 'dev',
  development :'dev',
  local: 'local',
};

const mode = mapping[process.env.NODE_ENV || 'local'] || process.env.NODE_ENV;
console.log("Loading configurations: ", mode);
module.exports = require('./' + mode +'/' + mode);