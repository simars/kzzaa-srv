module.exports = {
  "server": {
    host: '0.0.0.0',
    port: 8080,
    clustered: 'redis',
    protocol: "http", //TODO make it https when we have support for it
    link: process.env.LINK || "www.mydomain.com"
  },
  twitter: {
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET
  }

};