'use strict';

const config = require('./config');

module.exports = {
    "server": {
        "protocol": config.server.protocol,
        "host": config.server.link
    },
    "facebook": {
        "key": "[APP_ID]",
        "secret": "[APP_SECRET]",
        "callback": "/handle_facebook_callback",
        "scope": [
            "user_groups",
            "user_likes"
        ]
    },
    "twitter": {
        "key": config.twitter.consumer_key,
        "secret": config.twitter.consumer_secret,
        "callback": "/handle_twitter_callback"
    }
};