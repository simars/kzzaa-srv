'use strict';

let co = require('co'),
env = require('../config/environment'),
Twitter = require('twitter');


var twitter = function(token, secret) {

    console.log(token);

    var client = new Twitter({
        consumer_key: env.twitter.consumer_key,
        consumer_secret: env.twitter.consumer_secret,
        access_token_key: token,
        access_token_secret:    secret
    });
    return wrapper(client);
};

module.exports = twitter;







