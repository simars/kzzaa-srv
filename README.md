# README #

NodeJS Runtime + Koa App + Redis (Session) + Mongo DB (mongoose)

### What is this repository for? ###

* Server Side Development
* Docker orchestration

### How do I get set up? ###

__Development Environment__

Any environemnt that supports __docker__ (Windows 10 Pro, Mac or Linux)

__IDE and Code Style__

Use any IDE of your choice, recommended are WebStorm, IntelliJ
Coding Conventions
* User two spaces for tabs and all indentations
* Perfer single quotes for js string literals.

__Install dependencies__:

* docker
* docker-compose
* Windows user [checkout](#windows)

__Configure Environment__

* Set environment variable NODE_ENV
* Valid configuration local, dev, prod etc, 
* See config/environment/* for local vs prod vs dev overrides to config/config.json
* You can add your config/environment/__username__/__.username.js__ (don't forget to prepend __.__ for gitingore
* Set the NODE your enviornment
 * Windows __set NODE_ENV=dev__
 * Mac/Linx __export NODE_ENV=dev__
* Set your static files / content folder (where index.html exists)
 * Windows __set STATIC_CONTNET_DIR=crypto/crypto-ui/dist__
 * Mac/Linx __export STATIC_CONTNET_DIR=crypto/crypto-ui/dist__


__Run project__

```
mkdir crypto
cd crypto
git clone <project-url>/crypto-srv
cd crypto-srv
docker-compose up -d mongo
docker-comose up -d redis
mocha
node index.js
```

will run the http server at http://localhost:<port> where port dependes on the config used.

### Contribution guidelines ###

* Develop is feature branches
* Write tests
 * Mocha Runner
 * Super test
* Add postman collections to .postman folder for new API
* Successful Build before merge
* Code review before merge

<div id="windows"/>

### Are you windows user ? ###

**Setup your node environment**
  1. Use [nvm for Windows (node-version manager)](https://github.com/coreybutler/nvm-windows) 
  2. Open CMD or Power-Shell preferably with administrative privileges and run following commands  
  3. _nvm install latest_  (this should install Nodejs 8+should be installed.)
  4. _nvm list_ (should show you which is the latest version installed, let say it is 8.x.x)
  5. _nvm use 8.x.x_
  6. _node --version_
  

**Set up native node packages and windows build tools**
  1. Run powershell with administrator rights and execute following command.
  2. npm install --global --production windows-build-tools
  3. This will install python 2.7 and windows build tools. 
  4. Check this out for [issues with windows-build tools see](https://github.com/felixrieseberg/windows-build-tools/issues/57)
  5. The installation may take some time and you may need to restart the system, It would be better to restart the system once installation is over.
  6. set powershell path if not already set:  %SystemRoot%/system32/WindowsPowerShell/v1.0 to the path variable
  7. Make sure powershell is set in environment PATH.
  8. npm install --global node-gyp