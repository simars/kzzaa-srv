'use strict';

let mongoose      = require('mongoose'),
    Schema        = mongoose.Schema;


const AuthenticationSchema = new Schema ( {
    uid: {
        type: String
    },
    provider: {
        type: String,
        required : true
    },
    token: {
        type: String,
        required: true
    },
    secret: {
        type: String,
        required: true
    },
    date: {
        type: Date
    },
    userid : {
        type: Number,
        required: true
    }
});


AuthenticationSchema.pre('save', function (done) {
  console.log('Authentication presave: ', this);
  const auth = this;
  (async function () {
    {
      try {
        auth.date = Date.now();
        done();
      } catch (err) {
        done(err);
      }
    }
  })()
});


module.exports = mongoose.model('Authentication', AuthenticationSchema);