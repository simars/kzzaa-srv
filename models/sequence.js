'use strict';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

function sequenceGenerator(name) {
  let SequenceSchema = new Schema({
    next: {type: Number, default: 1}
  });

  let Sequence = mongoose.model(name + 'Seq', SequenceSchema);

  return {
    next: async () => {
      let sequence = await Sequence.find().exec();

      if (sequence.length < 1) {
        console.log('creating new sequence: ' + name);
        let seq = await Sequence.create({});
        return seq.next;
      } else {
        console.log('using existing sequence: ' + name);
        let seq = await Sequence
          .findByIdAndUpdate(sequence[0]._id,
            {$inc: {next: 1}},
            {new: true})
          .exec();
        return seq.next;
      }
    }
  };
}

module.exports = sequenceGenerator;
