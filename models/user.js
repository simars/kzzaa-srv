'use strict';

let mongoose      = require('mongoose'),
    Schema        = mongoose.Schema,
    bcrypt        = require('bcryptjs'),
    userSequence  = require('./sequence')('User'),
    Authentication = require('./authentication'),
    Twitter       = require('../bus/twitter');

mongoose.Promise = global.Promise;

const UserAttrSchema = new Schema({
  attribute: {
    type: String,
    required: true,
    lowercase: true,
  },
  value: {
    type: String,
    required: true
  }
});

const UserSchema = new Schema({
    username: {
      type      : String,
      required  : true,
      unique    : true,
      lowercase : true
    },
    password: {
      type      : String,
      required  : true
    },
    userid: {
      type      : Number,
      unique    : true
    },
    saved: {
      type      : [Schema.ObjectId],
      default   : []
    },
    liked: {
      type      : [Schema.ObjectId],
      default   : []
    },
    followed: {
      type      : [Schema.ObjectId],
      default   : []
    },
    name: {
      type      : String
    },
    country: {
      type      : String
    },
    email: {
      type      : String
    },
    attributes: [UserAttrSchema]  // ex [{phone: "+. .*" }, {city: ".*"}, {address: ".*"} ]

  },
  {
    toJson: {
      transform: function (doc, ret, options) {
        delete ret.password;
      }
    }
  });


UserSchema.pre('save', function(done) {
  const user = this;
  (async () => {
    try {
      user.userid = user.userid || await userSequence.next();
      await user.setPassword();
      done();
    } catch (err) {
      done(err);
    }
  })();
});

UserSchema.pre('update', function (done) {
  const user = this;
  if(!user.isModified('password')) {
    return done();
  }
  (async () => {
    try {
      await user.setPassword();
      return done();
    } catch (err) {
      done(err);
    }
  })();
});

UserSchema.methods.setPassword = function (password) {
  const user = this;
  return ( async () => {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash((user.password || password), salt);
    user.password = hash;
  })();
};


UserSchema.methods.comparePasswords = function(password) {
  const user = this;
  return (async() => {
    return await bcrypt.compare(password, user.password);
  })();
};

UserSchema.statics.passwordMatches = function(username, password){
  const User = this;
  return (async () => {
    const user = await User.findOne({ username: username.toLowerCase() }).exec();

    if (!user) {
      throw new Error('User not found.');
    }

    if (await user.comparePasswords(password)) {
      return user;
    }

    throw new Error('Password does not match.');
  })();

};

UserSchema.statics.load = function(id) {
  const User = this;
  return (async () => {
    const user = await User.findOne({ _id : id }).exec();
    if (!user) {
      throw new Error('User not found.');
    }
    return user;
  })()
};

UserSchema.virtual('twitter').get(async () => {
  if(!this._twitter) {
    let twitter = null;
    let authentication = await Authentication.findOne({userid: this.userid, provider: 'twitter' }).exec();
    if(authentication)  {
      twitter = Twitter(authentication.token, authentication.secret);
    }
    this._twitter = twitter;
    return twitter;
  }
  return this._twitter;
});

module.exports = mongoose.model('User', UserSchema);


