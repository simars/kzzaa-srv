'use strict';

const mongoose      = require('mongoose'),
    Schema        = mongoose.Schema,
    User          = require('../models/user'),
    config           = require('../config/config'),
    postSequence  = require('./sequence')('Post');

const PostSchema = new Schema({
  title: {
    type      : String,
    required  : true
  },
  description: {
    type      : String,
    required  : true
  },
  username: {
    type      : String,
    required  : true
  },
  userid: {
    type      : Number,
    required  : true
  },
  postid: {
    type      : Number
  },
  liked: {
    type      : [Number],
    defailt   : []
  },
  text: {
    type      : Boolean
  },
  date: {
    type      : Date
  },
  accountid: {
    type      : Number
  },
  type: {
    type      : String,
    required  : true
  },
  attachment: {
    fileId    : { type: Schema.ObjectId },
    filename  : { type: String },
    type      : { type: String }
  }
});

PostSchema.pre('save', function (done) {
  console.log('post presave: ', this);
  const post = this;
  (async () => {
    try {
      post.postid = await postSequence.next();
      post.date = new Date();
      done();
    } catch (err) {
      done(err);
    }
  })();
});

PostSchema.post('save', function (done) {
  console.log('post postsave: ', this);
  const post = this;
  (async ()=> {
    try {
      let user = await User.findOne({userid: post.userid}).exec();
      let twitter = await user.twitter;
      if(twitter) {
        console.log("posting to twitter", twitter);
        let response = await twitter.post('statuses/update', {status: post.title +
          ' Read more at ' + config.server.link + '/content/'+ post.postid.toString()});
        console.log("posted to twitter", response);
      }
      done();
    } catch (err) {
      console.log(err);
      done(err);
    }
  })();
});

module.exports = mongoose.model('Post', PostSchema);


