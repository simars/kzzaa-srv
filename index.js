const Session = require('./mw/session'),
  ResponseTime = require('./mw/response-time'),
  Logger = require('./mw/logger'),
  Content = require('./mw/content'),
  CORS = require('kcors'),
  config = require('./config/config'),
  mount   = require('koa-mount'),
  Grant   = require('grant-koa'),
  grant = new Grant(require('./config/grant'));
Auth = require('./mw/auth');
const Koa = require('koa');
const app = new Koa();


app.keys = ['ADAM', 'SIMAR'];


// handle static content & CORS
if(config.staticContentDir) app.use(Content());
if(config.server.cors) app.use(CORS());

// x-response-time
app.use(ResponseTime());

// logger
app.use(Logger());

// Session
app.use(Session());

//authentication
app.use(Auth.auth);

require('./persist')();
// mount APIs
require('./api/api')(app);
app.use(mount(grant));


const ip = config.server.host;
const port = config.server.port;
console.log('starting on : ',ip ,port );
//listen
let server = app.listen(port, ip);

// export the app for mocha / supertest
module.exports = server;
