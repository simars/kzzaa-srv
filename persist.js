'use strict';

let mongoose  = require('mongoose'),
    User      = require('./models/user'),
    config = require('./config/config');

function persist () {
  mongoose.Promise = global.Promise;

  mongoose.connect(config.mongodb.url, function (err, db) {
    if (err)
      throw(err);
  });

  User.findOne({ username: 'admin' }, function (err, docs) {
    if (err || !docs) {
      let user = new User({
        username: config.mongodb.username,
        password: config.mongodb.password
      });

      user.save();
    }
  });
}

module.exports = persist;
