'use strict'

const _ = require('lodash'),
  uid = require('uid-safe'),
  jwt = require('jsonwebtoken'),
  config = require('../config/config'),
  redisStore = require('koa-redis');



class Session {

  constructor(store, secret, sessionTtl, cookieOptions) {
    this.store = store || {
      get: function (key) {
        return this[key];
      },
      set: function (key, value) {
        this[key] = value;
      },
      destroy: function () {
        delete this[key];
      }
    };

    this.secret = secret || 'EASYONE'; // TODO , get secret from env config
    this.sessionTtl = sessionTtl || 24 * 60 * 60 * 1000; // TODO - get from env config (dev, prod etc)
    this.cookieOptions = cookieOptions || {
      httpOnly: true,
      path: '/',
      overwrite: true,
      signed: true,
      maxAge: this.sessionTtl
    };
  }

  async loadSession(ctx) {
    // get (session & authentication) from the header or cookie (cookie is for "keep me logged in")
    let token = ctx.get("Session") || ctx.cookies.get("Session");
    let auth = ctx.get('Authorization') || ctx.cookies.get('Authorization');
    let sessionId;
    if (token) {
      try {
        sessionId = jwt.verify(token, this.secret).id;
        console.log("Got session ID: ", sessionId);
        ctx.session = await this.store.get(sessionId);
        console.log("Got session: ", ctx.session);
      } catch (err) {
        console.log(err);
        ctx.cookies.set("Session", '');
        ctx.cookies.set('Authorization','');
        ctx.session = null;
        ctx.auth = null;
        sessionId = await uid(24);
        token = jwt.sign({id: sessionId}, this.secret);
      }
    } else {
      sessionId = await uid(24);
      token = jwt.sign({id: sessionId}, this.secret);
    }
    if (!ctx.session) {
      ctx.session = {id: sessionId};
    }
    ctx.set("Session", token);
    ctx.cookies.set("Session", token, this.cookieOptions);
    ctx.cookies.set('Authorization', auth);
    return sessionId;
  }

  async saveSession(ctx, sessionId) {
    if (ctx.session) {
      await this.store.set(sessionId, ctx.session, this.sessionTtl);
    }
    else {
      console.log("Clear Session");
      await this.store.destroy(sessionId);
    }
  }

}

module.exports = () => {
  const session = config.server.clustered?
    new Session(redisStore(config.redis)) :
    new Session();
  return async (ctx, next) => {
    const sessionId = await session.loadSession(ctx);

    await next();

    if (sessionId || ctx.request.method !== "HEAD") {
      await session.saveSession(ctx, sessionId);
    }
  };

};
