'use strict'

const
  jwt = require('jsonwebtoken'),
  secret = process.env.JWT_SECRET || 'EASYONE';

async function hasAuth(ctx, next) {
  if(ctx.user && ctx.user.userid) {
    await next();
  } else {
    ctx.throw(401, "Must be logged in");
  }
}


async function auth(ctx, next) {
  let authHeader, token, elements, scheme;
  authHeader = ctx.get('Authorization') || ctx.cookies.get('Authorization');

  console.log("Authheader", authHeader);

  if (authHeader) {

    elements = authHeader.split(' ');
    if (elements.length === 2) {

      scheme = elements[0];
      if (scheme === 'Bearer') {

        token = elements[1];
        try {

          ctx.user = jwt.verify(token, secret);
          console.log("USER",ctx.user);

        } catch(err) {
          console.log(err);
        }
      }
    }
  }
  await next();
}


module.exports = {
  hasAuth: hasAuth,
  auth: auth
};
