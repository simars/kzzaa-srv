'use strict'

const koaStatic = require('koa-static'),
        config = require('../config/config');

module.exports = () => {
    let dir = config.staticContentDir;
    console.log("config.staticContentDir-> " + (dir? "served from: " + dir : "Expecting (web-server) to serve static content)") );
    return koaStatic(dir, {defer: true});
};
