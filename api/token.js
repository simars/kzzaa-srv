'use strict'

const router = require('koa-router')();

router.get('/token', async (ctx, next) => {
  ctx.body = ctx.session;
  await next();
  ctx.response.status = 200;
});

module.exports = router;
