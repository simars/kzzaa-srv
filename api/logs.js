'use strict'
const Router = require('koa-router'),
  logs = new Router(),
  auth = require('../mw/auth');

logs.get('/logs', async (ctx, next) => {
  ctx.body = [{
    id: 1,
    description: 'Adam has a brilliant idea - Crypto OS!',
    date: new Date() - 60000 * 50
  }, {
    id: 2,
    description: 'Simar and Jashan have started development!',
    date: new Date()
  }];
  await next();
  ctx.response.status = 200;
});

module.exports = logs;
