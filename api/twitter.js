'use strict';

const Router = require('koa-router'),
  twitter = new Router(),
  User = require('../models/user');


twitter.get('/twitter/status/:username', async (ctx, next) => {
  let user = await User.findOne({userid: ctx.user.userid}).exec();
  console.log(user);
  let twitter = await user.twitter;
  console.log(twitter);
  let response = await twitter.get('statuses/user_timeline', {screen_name: ctx.params.username});
  var json = response[0];
  // var raw = response[1];
  console.log("Got JSON Response: " + json);
  ctx.body = json;
  ctx.response.status = 200;
  await next();
});

module.exports = twitter;
