'use strict'

const Router = require('koa-router'),
  parse = require('body'),
  jwt = require('jsonwebtoken'),
  auth = new Router(),
  User = require('../models/user'),
  koaBody = require('koa-body')(),
  secret = process.env.JWT_SECRET || 'EASYONE';


auth.post('/authenticate', koaBody, async (ctx, next) => {
  try {
    const payload = ctx.request.body;
    const user = await User.passwordMatches(payload.username, payload.password);
    if (user) {
      const claim = {userid: user.userid};
      ctx.body = {token: jwt.sign(claim, secret)};
      await next();
    } else {
      ctx.throw(401, 'Invalid')
    }
  } catch (err) {
    console.log('System error', err)
    ctx.throw(401, 'Unauthorized')
  }
});

module.exports = auth;
