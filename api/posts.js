'use strict';

const Router = require('koa-router'),
  mongoose = require('mongoose'),
  path = require('path'),
  _ = require('lodash'),
  Grid = require('gridfs-stream'),
  posts = new Router(),
  Post = require('../models/post'),
  User = require('../models/user'),
  auth = require('../mw/auth'),
  koaBody = require('koa-body'),
  fs = require('fs-promise');

Grid.mongo = mongoose.mongo;
let gfs = new Grid(mongoose.connection.db);

posts.get('/posts', auth.hasAuth, async (ctx, next) => {
  let types = {
    text: 'text',
    image: 'image',
    audio: 'audio',
    video: 'video',
    file: 'file'
  };

  let offset = Math.max(0, ctx.query.offset || 0);
  let count = Math.max(50, ctx.query.count || 0);
  let filter = {};

  if (ctx.query.type) {

    filter.type = ctx.query.type;
  }

  let posts = await Post.find(filter).sort({$natural: -1})
    .skip(offset).limit(count).exec();


  ctx.body = posts || [];
  await next();
  ctx.response.status = 200;
});

posts.get('/posts/saved', auth.hasAuth, async (ctx, next) => {
  let offset = Math.max(0, ctx.query.offset || 0);
  let count = Math.max(5, ctx.query.count || 0);
  let user = await User.findOne({userid: ctx.user.userid}).exec();
  let length = user.saved.length;

  let posts = await Post.find({
    _id: {
      $in: user.saved.slice(Math.max(0, length - count - offset), length - offset)
    }
  }).exec();


  ctx.body = posts || [];
  await next();
  ctx.response.status = 200;
});

posts.get('/posts/liked', auth.hasAuth, async (ctx, next) => {
  let offset = Math.max(0, ctx.query.offset || 0);
  let count = Math.max(5, ctx.query.count || 0);

  let user = await User.findOne({userid: ctx.user.userid}).exec();

  let length = user.liked.length;

  let posts = await Post.find({
    _id: {
      $in: user.liked.slice(Math.max(0, length - count - offset), length - offset)
    }
  }).exec();


  ctx.body = posts || [];
  await next();
  ctx.response.status = 200;
});

posts.get('/posts/:id/blob/:name', auth.hasAuth, async (ctx, next) => {
  let post = await Post.findOne({postid: ctx.params.id}).exec();

  let file = await gfs.files.findOne({
    _id: new mongoose.Types.ObjectId(post.attachment.fileId)
  });

  if(null == file ) {
    ctx.throw(401, "not found");
  }

  let readStream = gfs.createReadStream({
    _id: new mongoose.Types.ObjectId(post.attachment.fileId)
  });

  let that = ctx;


  that.set('Content-Length', file.length);
  that.type = path.extname(file.filename);
  that.body = readStream;
  await next();

});

posts.post('/posts/:id/blob', auth.hasAuth, koaBody({multipart: true}), async (ctx, next) => {
  if (!ctx.request.is('multipart/*')) {
    console.error('blob requires a multipart body');
    return await next();
  }

  let post = await Post.findOne({postid: ctx.params.id}).exec();
  let files = ctx.request.body.files || {};

  let types = {
    imageFile: 'image',
    videoFile: 'video',
    audioFile: 'audio',
    binaryFile: 'file'
  };

  for (let key in files) {
    const file = files[key];
    const reader = fs.createReadStream(file.path);
    let writer = gfs.createWriteStream({
      _id: new mongoose.Types.ObjectId(post.attachment.fileId),
      filename: file.name,
      mode: 'w',
      metadata: {
        userid: ctx.user.userid,
        postid: post.postid
      }
    });
    reader.pipe(writer);
  }
  ctx.response.status = 200;
  await next();
});

posts.post('/posts', auth.hasAuth, koaBody(), async (ctx, next) => {
  let body = ctx.request.body;

  let user = await User.findOne({userid: ctx.user.userid}).exec();


  body.userid = user.userid;
  body.username = user.username;

  if (body.type !== 'text') {
    body.attachment.fileId = new mongoose.Types.ObjectId();
  }

  let post = new Post(body);

  post = await post.save();
  ctx.body = post;
  ctx.response.status = 200;
  await next();
});

posts.post('/posts/:id/save', auth.hasAuth, async (ctx, next) => {
  let post = await Post.findOne({postid: ctx.params.id}).exec();

  let user = await User.findOneAndUpdate({
    userid: ctx.user.userid
  }, {
    $addToSet: {
      saved: new mongoose.Types.ObjectId(post._id)
    }
  }).exec();

  ctx.body = user;
  ctx.response.status = 200;
  await next();
});

posts.post('/posts/:id/like', auth.hasAuth, async (ctx, next) => {
  let post = await Post.findOneAndUpdate({
    postid: ctx.params.id
  }, {
    $addToSet: {
      liked: ctx.user.userid
    }
  });

  let user = await User.findOneAndUpdate({
    userid: ctx.user.userid
  }, {
    $addToSet: {
      liked: new mongoose.Types.ObjectId(post._id)
    }
  }).exec();

  ctx.body = user;
  ctx.response.status = 200;
  await next();
});

posts.get('/posts/:id', auth.hasAuth, async (ctx, next) => {
  let post = await Post.find({postid: ctx.params.id}).exec();

  if (post) {
    ctx.body = post;
  } else {
    ctx.throw(404, 'Post not found.');
  }

  await next();
  ctx.response.status = 200;
});

posts.delete('/posts/:id', auth.hasAuth, async (ctx, next) => {
  await Post.find({postid: ctx.params.id}).remove().exec();
  await next();
  ctx.response.status = 200;
});

module.exports = posts;
