'use strict';

const Router = require('koa-router'),
  users = new Router(),
  User = require('../models/user'),
  auth = require('../mw/auth'),
  koaBody = require('koa-body')(),
  _ = require('lodash');

users.get('/users', async (ctx, next) => {
  let users = await User.find().exec();
  ctx.body = users || [];
  await next();
  ctx.response.status = 200;
});

users.post('/users', koaBody, async (ctx, next) => {
  const payload = ctx.request.body;
  console.log(payload);
  const user = new User(payload);
  try {
    await user.save();
  } catch (err) {
    ctx.throw(404, "Could not save user: " + payload.username);
  }
  await next();
  ctx.response.status = 200;
});

users.get('/user/:id', auth.hasAuth, async (ctx, next) => {
  const user = await User.find({userid: ctx.params.id}).exec();
  if (!user) {
    ctx.throw(404, "User not found with id: " + ctx.params.id);
  }
  ctx.body = user;
  await next();
  ctx.response.status = 200;
});


users.delete('/user/:id', auth.hasAuth, async (ctx, next) => {
  await User.find({userid: ctx.params.id}).remove().exec();
  ctx.body = user;
  await next();
  ctx.response.status = 200;
});

users.get('/users/me', auth.hasAuth, async (ctx, next) => {
  try {
    const user = await User.findOne({userid: ctx.user.userid}).exec();
    await user.populate("attributes");
    ctx.body = user;
  } catch (err) {
    console.log("Failed user read", ctx.user.userid, err);
    ctx.throw(404, "Could not save user: " + ctx.user.userid);
  }
  await next();
  ctx.response.status = 200;
});

users.delete('/users/me', auth.hasAuth, async (ctx, next) => {
  await User.find({userid: ctx.user.userid}).remove().exec();
  await next;
  ctx.response.status = 200;
});

users.post('/users/me', auth.hasAuth, koaBody, async (ctx, next) => {
  const payload = ctx.request.body;
  delete payload.userid;
  delete payload.username;
  delete payload.password;
  try {
    await User.findOneAndUpdate({userid: ctx.user.userid}, payload).exec();
    const user = await User.findOne({userid: ctx.user.userid}).exec();
    delete user.password;
    ctx.body = user;
  } catch (err) {
    console.log("Failed user save", ctx.user.userid, err);
    ctx.throw(404, "Could not save user: " + ctx.user.userid);
  }
  await next();
  ctx.response.status = 200;
});

module.exports = users;
