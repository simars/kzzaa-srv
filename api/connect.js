'use strict';

const Router = require('koa-router'),
  connect = new Router(),
  User = require('../models/user'),
  Authentication = require('../models/authentication');


connect.get('/handle_twitter_callback', async (ctx, next) => {
  console.log(ctx.query);
  if (ctx.user) {
    let user = await User.findOne({userid: ctx.user.userid}).exec();
    let authentication = await Authentication.findOne({userid: user.userid, provider: 'twitter'}).exec();
    authentication = authentication || new Authentication();
    authentication.userid = user.userid;
    authentication.provider = "twitter";
    authentication.token = ctx.query.access_token;
    authentication.secret = ctx.query.access_secret;
    await authentication.save();
    ctx.body = "Twitter connected";
  } else {
    // TO DO (redirect to login page)
    ctx.body = JSON.stringify(ctx.query, null, 2)
  }
  await next();
  ctx.response.status = 200;

});

connect.get('/handle_facebook_callback', async (ctx, next) => {
  console.log(ctx.query);
  ctx.body = JSON.stringify(ctx.query, null, 2)
});

module.exports = connect;