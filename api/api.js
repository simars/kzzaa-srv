'use strict';

const users = require('./users'),
  posts = require('./posts'),
  auth = require('./auth'),
  logs = require('./logs'),
  twitter = require('./twitter'),
  connect = require('./connect'),
  token = require('./token');

module.exports = function (app) {
  app.use(users.middleware());
  app.use(posts.middleware());
  app.use(auth.middleware());
  app.use(logs.middleware());
  app.use(twitter.middleware());
  app.use(connect.middleware());
  app.use(token.middleware());

};
