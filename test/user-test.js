'use strict';
const request = require('supertest'),
   _ = require('lodash'),
  server = require('../index');

require('mocha');

describe('Testing User Scenarios', () => {
  describe('User Updates his profile', () => {
    let agent;
    let username;
    let authToken;
    before(async () => {
      agent = request.agent(server);
      await agent
        .get('/');
    });
    it("Create a new user", async () => {
      username = "user" + Math.random() * 20000;
      const response = await agent
        .post('/users')
        .send({username: username, password: "easyone"})
        .set("content-type", "application/json")
        .expect(200);
      
    });
    it("Authenticate the user", async () => {
      const response = await agent
        .post('/authenticate')
        .send({username: username, password: "easyone"})
        .set("content-type", "application/json")
        .expect(200);
      authToken = response.body.token;
      
    });
    it("Update user email and country", async () => {
      const response = await agent
        .post('/users/me')
        .send({country: 'INDIA', email: username+"@test.crypto.com"})
        .set("content-type", "application/json")
        .set("Authorization", "Bearer " + authToken)
        .expect(200);
      
    });
    it("Add some attributes to user", async () => {
      const response = await agent
        .post('/users/me')
        .send({attributes:
          [{ attribute : "phone" , value: "+1 111 1111 1111"}, { attribute: "city",  value : "Ludhiana" }]})
        .set("content-type", "application/json")
        .set("Authorization", "Bearer " + authToken)
        .expect(200);
      
    });
    it("It persist changes (updated email, country and attributes)", async () => {
      const response = await agent
        .get('/users/me')
        .set("content-type", "application/json")
        .set("Authorization", "Bearer " + authToken)
        .expect(200);
      const usr = response.body;
      
      if(username+"@test.crypto.com" !== usr.email) {
        throw new Error("Email mismatch " + usr.email)
      }
      if("INDIA" !== usr.country) {
        throw new Error("Email mismatch " + usr.country)
      }
      const attrs = usr.attributes;
      if(!attrs || attrs[0].value !==  "+1 111 1111 1111" ) {
        throw new Error("Attrs mismatch " + usr.attributes[0].value)
      }

    });
    after(async () => {
      const response = await agent.delete('/users/me')
        .set('Authorization', "Bearer " + authToken)
        .expect(200);
      
    })

  });
});
