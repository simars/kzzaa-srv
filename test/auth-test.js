'use strict';
let request = require('supertest'),

    server = require('../index');

require('mocha');

describe('Testing User Scenarios', () => {
    describe('User Signs up then Signs In to Cancel', () => {
        let agent;
        let username;
        let authToken;
        before(async () => {
            agent = request.agent(server);
            await agent
                .get('/');
        });
        it("Create a new user", async () => {
            username = "user" + Math.random() * 20000;
            const response = await agent
                .post('/users')
                .send({username: username, password: "easyone"})
                .set("content-type", "application/json")
                .expect(200);
            
        });
        it("Authenticate the user", async () => {
            const response = await agent
                .post('/authenticate')
                .send({username: username, password: "easyone"})
                .set("content-type", "application/json")
                .expect(200);
            authToken = response.body.token;
            
        });
        after(async () => {
            const response = await agent.delete('/users/me')
                .set('Authorization', "Bearer " + authToken)
                .expect(200);
            
        })

    });
});
