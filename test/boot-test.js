'use strict';
let request = require('supertest'),

server = require('../index');

require('mocha');

describe('Testing App boot',  () => {
    describe('Make post, get,delete HTTP requests', async () => {
        let agent;
        before(async () => {
            agent = request.agent(server);
            await agent
                .get('/');
        });
        it("Test end point logs should give response", (done) => {
            agent
                .get('/logs')
                .expect(200)
                .end(function (error, response) {
                    if (error)  return done(error);
                    const payload = response.body;
                    if(!payload) return done("Payload empty");
                    if(payload.length !== 2) return done("payload length must be 2");
                    if((payload[1].id - payload[0].id) !==1 ) return done("Items 1 and 2 not found");
                    done();
                });
        });
        it("Sticky sessions are created for requests for visitors", async () => {
            const response = await agent
                .get('/token')
                .expect(200);
            const response2 = await agent.get("/token")
                .expect(200);
            if(response.body.id !== response2.body.id) {
                throw new Error("Session ids of two consecutive requests unequal")
            }

        });
        after(async () => {
            await agent.delete('/')
                .expect(404);
        })

    });
});
